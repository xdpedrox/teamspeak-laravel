<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTsusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tsusers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('unique_id', 28);
            $table->smallInteger('owned_ch_id');
            $table->smallInteger('client_db_id');
            $table->integer('channel_deleted');

            $table->timestamps();
        });

        // `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
        // `unique_id` char(28) NOT NULL,
        // `owned_ch_id` smallint(5) unsigned DEFAULT NULL,
        // `client_db_id` smallint(5) unsigned NOT NULL,
        // `channel_deleted` int(11) unsigned DEFAULT NULL,
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tsusers');
    }
}
