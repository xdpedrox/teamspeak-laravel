<?php

namespace App\Services;

use GuzzleHttp\Client;
use App\Services\RequestHandler;
use App\Services\TeamSpeakCore;


class TeamSpeak
{

  protected $request;
  protected $ts3core;


  public function __construct(RequestHandler $request, TeamSpeakCore $ts3core)
  {
    $this->request = $request;
    $this->ts3core = $ts3core;
  }
  public function createUserChannel($user_channels_start, $user_channels_end)
  {
    $lastId = null;
    $gotStart = false;
    foreach ($this->request->getResponse('channellist') as $c) {

      if ($c->cid == $user_channels_start) {
        $gotStart = true;
      }

      if (!$gotStart || $c->pid != 0) {
        continue;
      }

      if ($c->cid == $user_channels_end) {
        break;
      }

      $lastId = $c->cid;
    }

    if (!isset($lastId)) {
      return false;
    }

    return $this->ts3core->createChannel($this->spacerCrc32('lol', time()), '123', null, $lastId, false);
  }

  function spacerCrc32($spacerName, $uniqueNum)
  {
    $crc32 = dechex(crc32($spacerName . $uniqueNum));

    return ctype_alpha($crc32[0]) ? '0' . $crc32 : $crc32;
  }
}
