<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="/css/main.css" rel="stylesheet">
</head>

<body>

  <div class="topbar">
    <div class="float_left">
      <table>
        <tbody>
          <tr>
            <td><span class="red">Slots: 168/500</span></td>
            <td><span class="grey">Uptime: 3 Days</span></td>
            <td><span class="red">Download: 20.66 GB</span></td>
            <td><span class="grey">Upload: 57.15 GB</span></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="float_right">
    </div>
  </div>
  <h1>Elite Portuguesa</h1>

  @yield('content')


  <footer>
    <p>Copyright 2020 Elite Portuguesa</p>
  </footer>
</body>
</html>