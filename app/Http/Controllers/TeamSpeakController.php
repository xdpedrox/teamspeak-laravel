<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\TeamSpeakCore;
use App\Services\TeamSpeak;

class TeamSpeakController extends Controller
{
  // use App\Pizza;

  protected $ts3;

  public function __construct(TeamSpeakCore $ts3core, TeamSpeak $ts3) {
    $this->ts3core = $ts3core;
    $this->ts3 = $ts3;
  }

  public function index() {

    // $pizzas = Pizza::latest()->get();      
    
    // $monitors = $this->ts3core->createChannel('12323', 'password', null, false);
    $monitors = $this->ts3->createUserChannel(148, 149, null, false);

    var_dump($monitors);
    return view('teamspeak.index');
  }

  public function show($id = 1) {
    
    // $pizza = Pizza::findOrFail($id);

    return view('teamspeak.show');
    // , ['pizza' => $pizza]);
  }

  public function create() {
    return view('teamspeak.create');
  }

  // public function store() {

  //   $pizza = new Pizza();

  //   $pizza->name = request('name');
  //   $pizza->type = request('type');
  //   $pizza->base = request('base');
  //   $pizza->toppings = request('toppings');

  //   $pizza->save();

  //   return redirect('/')->with('mssg', 'Thanks for your order!');

  // }

  // public function destroy($id) {

  //   $pizza = Pizza::findOrFail($id);
  //   $pizza->delete();

  //   return redirect('/pizzas');

  // }

}

