<?php

namespace App\Services;

use GuzzleHttp\Client;
use App\Services\RequestHandler;

class TeamSpeakCore
{
  protected $ts3;

  public function __construct(RequestHandler $handler)
  {
    $this->ts3 = $handler;
  }
  public function createChannel($name, $password, $parentID, $order, $public)
  {
    $permValue = 5;

    if ($public) {
    $default = array(
      'channel_flag_permanent' => 1,
      'channel_codec_quality' => 10,
      'channel_maxclients' => -1,
      'channel_maxfamilyclients' => -1,
      'channel_flag_maxclients_unlimited' =>  1,
      'channel_flag_maxfamilyclients_unlimited' => 1
    );
  } else {
    $default = array(
      'channel_flag_permanent' => 1,
      'channel_codec_quality' => 10,
      'channel_maxclients' => 0,
      'channel_maxfamilyclients' => 0,
      'channel_flag_maxclients_unlimited' =>  0,
      'channel_flag_maxfamilyclients_unlimited' => 0,
    );
  }


    $params = array(
      "channel_name" => $name,
      "channel_password" => $password,
      "cpid" => $parentID,
      "channel_order" => $order
    );
    
    $RequestParams = array_merge($params, $default);

    $response = $this->ts3->getResponse('channelcreate', $RequestParams);
    return (int) $response[0]->cid;
    
  }
}
